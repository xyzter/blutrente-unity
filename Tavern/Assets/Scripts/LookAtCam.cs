﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCam : MonoBehaviour
{
    Transform m_Camera;

    private void Start()
    {
        m_Camera = Camera.main.transform;
    }

    //Orient the camera after all movement is completed this frame to avoid jittering
    void LateUpdate()
    {
        //transform.LookAt(m_Camera);
        transform.rotation = Quaternion.Euler(0, -90, 0); 
    }
}
