﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class PlayerTrail : MonoBehaviour
{
    private float timeBtwSpawns;
    public float startTime;

    public GameObject echo;    

    NavMeshAgent agent;

    Vector3 mouse_pos;

    public Camera cam;


    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.hasPath)
        {
            Debug.Log("oi");
            if(timeBtwSpawns <= 0)
            {
                GameObject instance = (GameObject)Instantiate(echo, transform.position, Quaternion.identity);
                Destroy(instance, 1f);
                timeBtwSpawns = startTime;
            } else
            {
                timeBtwSpawns -= Time.deltaTime;
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            Transform target;
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.tag == "Table")
                {
                    target = hit.collider.gameObject.transform.GetChild(0).transform;
                    this.agent.SetDestination(target.position);
                    return;
                }
                this.agent.SetDestination(hit.point);
            }
        }

    }
}
