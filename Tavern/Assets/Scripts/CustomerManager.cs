﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class CustomerManager : MonoBehaviour
{
    Clock clock;
    GameManager gm;

    public GameObject table;
    NavMeshAgent agent;

    Animator anim;
    float time_to_order;

    bool waiting_table = true;
    public bool going_table;
    bool on_table;

    int random_line;
    Transform line;
    GameObject line_gameobject;


    public bool settled;
    public bool day_end;

    GameObject exit;
    GameObject dorms;

    public string order_name;

    //------------------------------
    [Header("Order")]
    bool ready_to_order;
    public bool ordered;
    float order_time;
    public int order;

    float patience;

    public GameObject beer;
    public GameObject soup;
    public bool waiting_for_order;

    public GameObject exclamation;
    bool is_eating;
    float eating;
    bool ate;

    public bool going_to_dorm;
    public bool chosen;


    int tip_amount;

    //FMOD

    [FMODUnity.EventRef]
    public string burp;

    bool line_called;

    void Start()
    {
        this.gameObject.name = "Customer";

        gm = FindObjectOfType<GameManager>();
        
        anim = GetComponent<Animator>();
        time_to_order = Random.Range(5, 15);
        agent = GetComponent<NavMeshAgent>();

        order_time = Random.Range(3, 7);
        SelectOrder();

        clock = FindObjectOfType<Clock>();

        exit = GameObject.Find("Exit");
        dorms = GameObject.Find("Dorms");

        GlobalControl.Instance.customers++;

        patience = 70f;

        random_line = (int)Random.Range(1, 5);
        line = this.transform.Find("FalaPos");

        clock.customers++;
    }

    void Update()
    {
       // Debug.Log(patience);

        if(agent.hasPath)
        {
            anim.SetTrigger("walk");
        } else
        {
            anim.SetTrigger("idle");
        }

        if (waiting_table && !going_table )
        {
            patience -= Time.deltaTime;

            if (patience <= 0) Leave();

            return;
        }

        if(going_table && !this.agent.hasPath)
        {
            patience = 60f;
            waiting_table = false;
            going_table = false;
            on_table = true;
            settled = true;
            return;
        }

        if(is_eating)
        {
            if (eating > 0)
            {
                eating -= Time.deltaTime;
                //if (!line_called) CallLine();
            }

            if(eating <= 0 && !ate)
            {
                settled = false;
                if(table != null)
                {
                    table.GetComponent<TableManager>().tip_amount = TipSum(tip_amount);
                    table.GetComponent<TableManager>().has_tip = true;
                    GlobalControl.Instance.tips++;
                    table.GetComponent<TableManager>().has_customer = false;
                    ate = true;
                }
                if (GlobalControl.Instance.dorm_current < GlobalControl.Instance.dorm_max)
                {
                    agent.SetDestination(dorms.transform.position);
                    FMODUnity.RuntimeManager.PlayOneShot(burp);
                    this.transform.parent = null;
                    Invoke("Exit", 8);
                    GlobalControl.Instance.customers--;
                    GlobalControl.Instance.dorm_current++;
                    clock.can_end = true;
                    going_to_dorm = true;
                    return;
                }
                else
                {
                    agent.SetDestination(exit.transform.position);
                    FMODUnity.RuntimeManager.PlayOneShot(burp);
                    this.transform.parent = null;
                    Invoke("Exit", 6);
                    GlobalControl.Instance.customers--;
                    clock.can_end = true;
                    return;
                }
            }
            return;
        }

        if (waiting_for_order)
        {
            patience -= Time.deltaTime;
            
             if (patience <= 0)
            {
                Leave();
                return;
            }
            

            DeactivateOrder(order);
            return;
        }

        if(on_table)
        {
            patience -= Time.deltaTime;

            
            if (patience <= 0)
            {
                Debug.Log("Vazei antes de pedir, so com a exclamacao");
                Leave();
            }
            

            if(order_time > 0) order_time -= Time.deltaTime;
            if (order_time <= 0)
            {
                ActivateOrder(order);
                return;
            }
            table.GetComponent<TableManager>().has_customer = true;
        }
    }

    void Leave()
    {
        this.transform.parent = null;
        Debug.Log("vazei");
        Invoke("Exit", 9);
        GlobalControl.Instance.customers--;
        clock.can_end = true;
        agent.SetDestination(exit.transform.position);
        return;
    }

    void ActivateOrder(int order_number)
    {
        if (order_number == 1)
        {
            exclamation.SetActive(true);
            ordered = true;
            return;
        }

        if (order_number == 2)
        {
            exclamation.SetActive(true);
            ordered = true;
            return;
        }
    }

    public void DeactivateOrder(int order_number)
    {
        patience = 70f;

        if (order_number == 1)
        {
            beer.SetActive(true);
            exclamation.SetActive(false);
            return;
        }

        if (order_number == 2)
        {
            soup.SetActive(true);
            exclamation.SetActive(false);
            return;
        }
    }

    public void HaveFood()
    {
        beer.SetActive(false);
        soup.SetActive(false);

        waiting_for_order = false;
        is_eating = true;
        eating = Random.Range(7, 11);
        return;
    }

    void Exit()
    {
        this.gameObject.SetActive(false);
    }

    public int TipSum(int x)
    {
        if(GlobalControl.Instance.food_one) x = 13;

        if (GlobalControl.Instance.food_two) x = 15;

        if (GlobalControl.Instance.food_three) x = 17;

        if (GlobalControl.Instance.drink_one) x += 0;

        if (GlobalControl.Instance.drink_two) x += 2;

        if (GlobalControl.Instance.drink_three) x += 3;

        if (GlobalControl.Instance.fireplace_one)
            x += 5;
        if (GlobalControl.Instance.fireplace_two)
            x += 7;
        if (GlobalControl.Instance.fireplace_three)
            x += 9;
        if (GlobalControl.Instance.rug)
            x += 3;


        return x;
    }

    void SelectOrder()
    {
        if(GlobalControl.Instance.food_one)
        {
            order = (int)Random.Range(1, 3);
            if (order == 1) order_name = "Beer";
            if (order == 2) order_name = "Soup";
            return;
        }
        /*
        else if(GlobalControl.Instance.food_two)
        {

        }
        */
    }
    
    void CallLine()
    {
        if(random_line == 1)
        {
            line_gameobject = (GameObject)Instantiate(Resources.Load("Fala"), line.position, Quaternion.Euler(0, 0, 0));
            line_gameobject.transform.parent = this.transform.parent;
            line_called = true;
            return;
        }
        if (random_line == 2)
        {
            line_gameobject = (GameObject)Instantiate(Resources.Load("Fala2"), line.position, Quaternion.Euler(0, 0, 0));
            line_gameobject.transform.parent = this.transform.parent;
            line_called = true;
            return;
        }
        if (random_line == 3)
        {
            line_gameobject = (GameObject)Instantiate(Resources.Load("Fala3"), line.position, Quaternion.Euler(0, 0, 0));
            line_gameobject.transform.parent = this.transform.parent;
            line_called = true;
            return;
        }
        if (random_line == 4)
        {
            line_gameobject = (GameObject)Instantiate(Resources.Load("Fala4"), line.position, Quaternion.Euler(0, 0, 0));
            line_gameobject.transform.parent = this.transform.parent;
            line_called = true;
            return;
        }
    }
    
}
