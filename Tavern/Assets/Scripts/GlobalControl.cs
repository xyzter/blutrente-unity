﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;

public class GlobalControl : MonoBehaviour
{
    public static GlobalControl Instance;

    public int speed_bough = 1;

    public bool level_one = true;
    public bool level_two;
    public bool level_three;

    public bool light_one = true;
    public bool light_two;
    public bool light_three;

    public bool food_one = true;
    public bool food_two;
    public bool food_three;

    public bool drink_one = true;
    public bool drink_two;
    public bool drink_three;

    public bool fireplace_one;
    public bool fireplace_two;
    public bool fireplace_three;
    public bool has_fireplace;

    public bool helper;

    public int deal_needed;
    public int deal_given;

    public bool rug;

    public int tips;
    public int customers;

    public int dorm_max;
    public int dorm_current;

    public int money;
    public int index = 2;

    public float speed;
    public float acceleration;
    public int max_orders;

    public int dorms_start_cost;

    void Awake()
    {
        dorms_start_cost = 75;

        transform.parent = null;

        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        /*
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
            Destroy(this.gameObject);
            return;
        }
        */
    }
}
