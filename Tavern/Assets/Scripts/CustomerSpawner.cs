﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerSpawner : MonoBehaviour
{

    public Transform[] places;
    int places_index;

    Clock clock;
    float time_to_next_customer;
    public int max_customers;
    int actual_customers;
    // Start is called before the first frame update
    void Start()
    {
        clock = FindObjectOfType<Clock>();
        time_to_next_customer = 1;
    }

    // Update is called once per frame
    void Update()
    {
        time_to_next_customer -= Time.deltaTime;

        //Debug.Log(GlobalControl.Instance.dorm_current + " / " + GlobalControl.Instance.dorm_max);

        if(clock.clock_time >= 0 && time_to_next_customer <= 0 && actual_customers < max_customers  )
        {
            Instantiate(Resources.Load("Cliente1"), places[places_index].position, Quaternion.Euler(0, 90, 0));
            actual_customers++;
            time_to_next_customer = Random.Range(5, 11);
            places_index++;
            if(places_index > places.Length - 1)
            {
                places_index = 0;
            }
        }
    }
}
