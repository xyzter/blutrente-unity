﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    public GameObject tutorial_text;
    public GameObject credits_text;

    [FMODUnity.EventRef]
    public string button;
    

    public void Play()
    {
        FMODUnity.RuntimeManager.PlayOneShot(button);
        SceneManager.LoadScene("Tutorial");
    }

    public void Tutorial()
    {
        tutorial_text.SetActive(true);
        FMODUnity.RuntimeManager.PlayOneShot(button);
    }

    public void TutorialBack()
    {
        FMODUnity.RuntimeManager.PlayOneShot(button);
        tutorial_text.SetActive(false);
    }

    public void Credits()
    {
        credits_text.SetActive(true);
        FMODUnity.RuntimeManager.PlayOneShot(button);
    }

    public void CreditsBack()
    {
        FMODUnity.RuntimeManager.PlayOneShot(button);
        credits_text.SetActive(false);
    }

    public void Exit()
    {
        FMODUnity.RuntimeManager.PlayOneShot(button);
        Application.Quit();
    }
}
