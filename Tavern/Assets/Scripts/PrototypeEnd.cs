﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PrototypeEnd : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if(Input.anyKeyDown)
        {
            Destroy(GlobalControl.Instance.gameObject);
            SceneManager.LoadScene("Menu");
        }
    }
}
