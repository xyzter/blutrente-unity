﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Clock : MonoBehaviour
{
    public GameObject day_end;

    Player player;

    private const float REAL_SECONDS_PER_INGAME_DAY = 60f;

    public float clock_time;

    GameManager gm;

    private Transform clockHandTransform;
    private float day;
    bool ended;

    CustomerSpawner customer_manager;

    public bool can_end;


    public int customers;
    private void Awake()
    {
        ended = false;
        player = FindObjectOfType<Player>();
        gm = FindObjectOfType<GameManager>();
        clock_time = REAL_SECONDS_PER_INGAME_DAY;
        day_end.SetActive(false);
        clockHandTransform = transform.Find("clockHand");
        customer_manager = FindObjectOfType<CustomerSpawner>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            Invoke("DayEnd", 1);
            day_end.SetActive(true);
            ended = true;
            return;
        }


        if(customers >= customer_manager.max_customers && GlobalControl.Instance.tips <= 0 && GlobalControl.Instance.customers <= 0)
        {

            day_end.SetActive(true);
            Invoke("DayEnd", 1);
            can_end = false;
            ended = true;
            return;
        }
    }

    void DayEnd()
    {
        gm.DayEnd();
        player.Save();
    }
    
}
