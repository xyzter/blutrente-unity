﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{

    public GameObject[] tutorials;
    public GameObject button;
    public GameObject image;
    int i = 0;

    public bool nosferatu;
    public bool end;

    bool tutorial_active = true;

    // Update is called once per frame
    void Update()
    {

        if(end)
        {
            Debug.Log(i + "\n" + (tutorials.Length - 1));
            if (Input.anyKeyDown)
            {

                if (i < tutorials.Length - 1)
                {
                    tutorials[i].SetActive(false);
                    i++;
                    tutorials[i].SetActive(true);
                }
                else
                {
                    tutorial_active = false;
                    button.SetActive(false);
                    image.SetActive(false);
                    i++;
                    tutorials[i].SetActive(false);
                }
            }
                if (i > tutorials.Length - 1)
                {
                    Debug.Log("oi");
                    EndGame();
                }
            return;
        }


        if(tutorial_active)
        {
            Time.timeScale = 0;

            tutorials[i].SetActive(true);
            button.SetActive(true);
            image.SetActive(true);

            if (Input.GetKeyDown(KeyCode.S) && !nosferatu)
            {
                tutorial_active = false;
                button.SetActive(false);
                image.SetActive(false);
                tutorials[i].SetActive(false);
                return;
            }

            if(Input.anyKeyDown)
            {
                if (i < tutorials.Length - 1)
                {
                    tutorials[i].SetActive(false);
                    i++;
                    tutorials[i].SetActive(true);
                }
                else
                {
                    tutorial_active = false;
                    button.SetActive(false);
                    image.SetActive(false);
                    tutorials[i].SetActive(false);
                }
            }
            return;
        }

        if(!tutorial_active)
        {
            Time.timeScale = 1;
            Destroy(this.gameObject);
        }
    }

    void EndGame()
    {
        SceneManager.LoadScene("EndGame");
    }
}
