﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HelperAI : MonoBehaviour
{

    GameObject[] tables;

    GameObject table_target;

    Transform target;
    NavMeshAgent agent;

    GameManager game_manager;

    GameObject helper_stay;

    GameObject customer_target;

    bool on_path;
    public bool day_end;

    float first_time_spawn = 3;
    float time_btw_refresh;


    //bool walking;

    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        game_manager = FindObjectOfType<GameManager>();
        agent = this.GetComponent<NavMeshAgent>();
        tables = GameObject.FindGameObjectsWithTag("Customer");
        time_btw_refresh = first_time_spawn;

        helper_stay = GameObject.Find("HelperStay");
        anim = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!on_path) time_btw_refresh -= Time.deltaTime;

        if(time_btw_refresh <= 0 && !on_path)
        {
            tables = GameObject.FindGameObjectsWithTag("Customer");

            foreach (GameObject item in tables)
            {
                if (item.GetComponent<CustomerManager>().ordered && item.GetComponent<CustomerManager>().table != null )
                {
                    //if (table_target != null) table_target.GetComponent<TableManager>().has_player = false;
                    customer_target = item.gameObject;
                    target = item.GetComponent<CustomerManager>().table.gameObject.transform.Find("WaiterPosition");
                    table_target = item.GetComponent<CustomerManager>().table;
                    table_target.GetComponent<TableManager>().has_player = true;
                    agent.SetDestination(target.position);
                    game_manager.order_number = item.GetComponent<CustomerManager>().order;
                    on_path = true;
                    return;
                }
            }
        }

        if(agent.hasPath)
        {
            anim.SetTrigger("walk");
        }

        if(!agent.hasPath)
        {
            anim.SetTrigger("idle");
        }

        if(on_path && !agent.hasPath)
        {
            //table_target.GetComponent<TableManager>().RandomOrder();
            Debug.Log("Foi");
            game_manager.Order(game_manager.order_number);
            game_manager.order_number = 0;
            customer_target.GetComponent<CustomerManager>().waiting_for_order = true;
            customer_target.GetComponent<CustomerManager>().ordered = false;
            time_btw_refresh = first_time_spawn;
            on_path = false;
            agent.SetDestination(helper_stay.transform.position);
        }
    }
}
