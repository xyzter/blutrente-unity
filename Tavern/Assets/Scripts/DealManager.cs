﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DealManager : MonoBehaviour
{
    public static DealManager Instance;

    public int deal_needed;

    public GameObject[] panos;

    int index = 0;

    private void Awake()
    {
        index = 0;
        GlobalControl.Instance.deal_needed = deal_needed;
        panos = GameObject.FindGameObjectsWithTag("Pano");
    }

    public void GiveChosenOne()
    {
        if (GlobalControl.Instance.deal_given < GlobalControl.Instance.deal_needed)
        {
            GlobalControl.Instance.deal_given++;
            panos[index].SetActive(false);
            Debug.Log("desativei");
            index++;
            Debug.Log(index + " aumentei um");
        }
    }
}
