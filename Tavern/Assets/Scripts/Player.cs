﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    //public PlayerStats localPlayerStats = new PlayerStats();
    //---------- Pegar Comida---------------
    Transform food_dest;
    Transform dest_one;
    Transform dest_two;
    bool destinationOne;
    bool destinationTwo;

    Transform order;
    bool get_food;
    bool getting_food;
    bool customer_order;
    bool has_food = false;

    //--------------------------------------

    Animator animator;

    bool first_time_clicking;

    //--------------Deal----------
    DealManager dealing;
    GameObject[] paninhos;
    int pano_index; 

    // TRANSFORMS

    GameObject[] positions;
    GameObject _temp;
    public GameObject[] orders_on_hand = new GameObject[3];
    bool pos_one, pos_two, pos_three;
    string[] orders;
    int k;
    public int max_order;
    int order_in_hand;


    //-----------------------

    bool getting_tip;
    bool going_to_table = false;
    

    GameManager game_manager;

    NavMeshAgent agent;

    //Transform target;
    GameObject table;
    GameObject customer_go;

    Vector2 mouse_pos;
    Transform target_agent;
    GameObject tableGameObject;

    public string wait_position;
    public int money;


    public Camera cam;
    bool on_path = false;
    bool customer_click = false;
    public bool day_end;


    //FMOD

    [FMODUnity.EventRef]
    public string gorjeta;
    [FMODUnity.EventRef]
    public string CustomerSelected;
    [FMODUnity.EventRef]
    public string TableSelected;
    [FMODUnity.EventRef]
    public string PickUpFood;
    [FMODUnity.EventRef]
    public string serving;
    [FMODUnity.EventRef]
    public string customer_ordered;
    [FMODUnity.EventRef]
    public string customer_given_to_nosferatu;

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        agent.speed = GlobalControl.Instance.speed;
        agent.acceleration = GlobalControl.Instance.acceleration;
        money = GlobalControl.Instance.money;

        max_order = GlobalControl.Instance.max_orders;

        positions = GameObject.FindGameObjectsWithTag("FoodDest");

        game_manager = FindObjectOfType<GameManager>();

        animator = this.GetComponent<Animator>();

        dealing = FindObjectOfType<DealManager>();

        paninhos = GameObject.FindGameObjectsWithTag("Pano");

        //dest_one = transform.Find("Destination_one");
        //dest_two = transform.Find("Destination_two");
    }

    // Update is called once per frame
    void Update()
    {
        Transform customer_target;

        if (Input.GetKeyDown(KeyCode.N)) game_manager.NextDay();

        if (Input.GetKeyDown(KeyCode.L)) Time.timeScale = 1;

        if (transform.childCount == 0)
            has_food = false;

        if (Input.GetMouseButtonDown(0))
        {

            Ray raycast = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            Transform target;
            if (Physics.Raycast(raycast, out hit))
            {
                if (hit.collider.gameObject.CompareTag("Order") && order_in_hand < max_order )
                {
                    agent.SetDestination(hit.point);
                    food_dest = null;
                    on_path = true;
                    get_food = true;
                    //order = hit.collider.gameObject.transform;
                    _temp = hit.collider.gameObject;
                    TestVectorFood();
                    return;
                }

                if (hit.collider.gameObject.CompareTag("Customer"))
                {
                    customer_go = hit.collider.gameObject;
                    if (hit.collider.GetComponent<CustomerManager>().settled) return;

                    if (customer_go.GetComponent<CustomerManager>().going_to_dorm == true && customer_go.GetComponent<CustomerManager>().chosen == false)
                    {
                        dealing.GiveChosenOne();
                        FMODUnity.RuntimeManager.PlayOneShot(customer_given_to_nosferatu);
                        customer_go.GetComponent<CustomerManager>().chosen = true;
                        customer_go.GetComponent<CustomerManager>().settled = true;
                        return;
                    }
                    else if (customer_go.GetComponent<CustomerManager>().settled == false)
                    {
                        FMODUnity.RuntimeManager.PlayOneShot(CustomerSelected);
                        customer_click = true;
                        return;
                    }
                    else if (customer_go.GetComponent<CustomerManager>().settled == true)
                    {
                        customer_go = null;
                        return;
                    }

                }
                if (customer_click && hit.collider.gameObject.CompareTag("Table"))
                {
                    if (hit.collider.GetComponent<TableManager>().has_customer) return;
                    if (hit.collider.GetComponent<TableManager>().has_tip) return;

                    customer_target = hit.collider.gameObject.transform.Find("Lugar1").transform;
                    customer_go.GetComponent<CustomerManager>().table = hit.collider.gameObject;
                    customer_go.GetComponent<NavMeshAgent>().SetDestination(customer_target.position);
                    customer_go.GetComponent<CustomerManager>().going_table = true;
                    customer_click = false;
                    customer_go.transform.parent = hit.collider.gameObject.transform;
                    FMODUnity.RuntimeManager.PlayOneShot(TableSelected);
                    return;
                }
                if (hit.collider.gameObject.CompareTag("Table") && hit.collider.gameObject.GetComponent<TableManager>().has_customer == true)
                {
                    if (customer_go != null) customer_go = null;

                    if (hit.collider.gameObject.transform.Find("Customer").GetComponent<CustomerManager>().ordered)
                    {
                        target = hit.collider.gameObject.transform.Find("WaiterPosition").transform;
                        customer_go = hit.collider.gameObject.transform.Find("Customer").gameObject;
                        food_dest = hit.collider.gameObject.transform.Find("FoodDest").transform;
                        target_agent = target;
                        tableGameObject = hit.collider.gameObject;
                        game_manager.order_number = customer_go.GetComponent<CustomerManager>().order;
                        this.agent.SetDestination(target.position);
                        on_path = true;
                        customer_order = true;
                        return;
                    }
                    else if (hit.collider.gameObject.transform.Find("Customer").GetComponent<CustomerManager>().waiting_for_order)
                    {
                        target = hit.collider.gameObject.transform.Find("WaiterPosition").transform;
                        customer_go = hit.collider.gameObject.transform.Find("Customer").gameObject;
                        food_dest = hit.collider.gameObject.transform.Find("FoodDest").transform;
                        target_agent = target;
                        tableGameObject = hit.collider.gameObject;
                        this.agent.SetDestination(target.position);
                        on_path = true;
                        going_to_table = true;
                        return;
                    }
                    target = hit.collider.gameObject.transform.Find("WaiterPosition").transform;
                    target_agent = target;
                    tableGameObject = hit.collider.gameObject;
                    this.agent.SetDestination(target.position);
                    on_path = true;
                    return;
                }

                if (hit.collider.gameObject.CompareTag("Table"))
                {
                    if (tableGameObject != null)
                    {
                        tableGameObject.GetComponent<TableManager>().has_player = false;
                        tableGameObject = null;
                        on_path = false;
                        return;
                    }
                    if (hit.collider.gameObject.GetComponent<TableManager>().has_tip)
                    {
                        target = hit.collider.gameObject.transform.Find("WaiterPosition").transform;
                        tableGameObject = hit.collider.gameObject;
                        target_agent = target;
                        this.agent.SetDestination(target.position);
                        on_path = true;
                        getting_tip = true;
                        return;
                    }
                    target = hit.collider.gameObject.transform.Find("WaiterPosition").transform;
                    target_agent = target;
                    this.agent.SetDestination(target.position);
                    on_path = true;
                    return;
                }

                this.agent.SetDestination(hit.point);
                get_food = false;
                customer_click = false;
                return;
            }
                

        }

        if (customer_click)
        {
            return;
        }

        /*
        if (order == null)
        {
            has_food = false;
        }
        */
        
        if(agent.hasPath)
        {
            animator.SetTrigger("walk");
        } else
        {
            animator.SetTrigger("idle");
        }
        
        if (on_path && !agent.hasPath)
        {
            if (get_food)
            {
                FMODUnity.RuntimeManager.PlayOneShot(PickUpFood);
                GetFood();
                get_food = false;
                order_in_hand++;
                has_food = true;
            }
            if (customer_order && customer_go.GetComponent<CustomerManager>().ordered)
            {
                game_manager.Order(game_manager.order_number);
                game_manager.order_number = 0;
                customer_go.GetComponent<CustomerManager>().waiting_for_order = true;
                customer_go.GetComponent<CustomerManager>().ordered = false;
                customer_order = false;
                FMODUnity.RuntimeManager.PlayOneShot(customer_ordered);
            }
            if (getting_tip && tableGameObject.GetComponent<TableManager>().has_tip == true)
            {
                FMODUnity.RuntimeManager.PlayOneShot(gorjeta);
                GetMoney();
                tableGameObject.GetComponent<TableManager>().has_tip = false;
                tableGameObject.GetComponent<TableManager>().tip_amount = 0;
                foreach (Transform item in tableGameObject.transform)
                {
                    if (item.gameObject.tag == "Order")
                        item.gameObject.SetActive(false);
                }
                getting_tip = false;
                GlobalControl.Instance.tips--;
            }
            /*
            if (has_food && customer_go.GetComponent<CustomerManager>().order == 1 && order != null &&
                customer_go.GetComponent<CustomerManager>().waiting_for_order && going_to_table && this.gameObject.transform.GetChild(1).name == "Beer")
            {
                FMODUnity.RuntimeManager.PlayOneShot(serving);
                order.parent = tableGameObject.transform;
                order.transform.position = food_dest.position;
                order.transform.rotation = food_dest.rotation;
                customer_go.GetComponent<CustomerManager>().HaveFood();
                has_food = false;
                destinationOne = false;
                going_to_table = false;
                order = null;
                customer_go = null;
                return;
            }
            */
            /*
            if (has_food && customer_go.GetComponent<CustomerManager>().order_name == order.gameObject.name && order != null &&
                customer_go.GetComponent<CustomerManager>().waiting_for_order && going_to_table)
            {
                LeaveFood();
            }
            */
            
            if (customer_go != null)
            {
                if (has_food && customer_go.GetComponent<CustomerManager>().waiting_for_order)
                {
                    for (int i = 0; i < max_order; i++)
                    {
                        if (orders_on_hand[i] != null)
                        {
                            if (orders_on_hand[i].name == customer_go.GetComponent<CustomerManager>().order_name)
                            {
                                LeaveFood(i);
                            }
                        }
                    }
                    
                }
            }
            customer_go = null;
            on_path = false;
            return;
        }
    }

    void LeaveFood(int index)
    {
        FMODUnity.RuntimeManager.PlayOneShot(serving);
        //order.parent = tableGameObject.transform;
        /*
        order.transform.position = food_dest.position;
        order.transform.rotation = food_dest.rotation;
        */
        orders_on_hand[index].gameObject.transform.parent = tableGameObject.transform;
        orders_on_hand[index].gameObject.transform.position = food_dest.position;
        orders_on_hand[index].gameObject.transform.rotation = food_dest.rotation;
        ResetBool(index);
        orders_on_hand[index] = null;

        customer_go.GetComponent<CustomerManager>().HaveFood();
        destinationOne = false;
        going_to_table = false;
        order = null;
        customer_go = null;
        k--;
        order_in_hand--;
        return;
    }


    void GetFood()
    {
        /*
        if (!destinationOne)
        {
            order.transform.parent = this.transform;
            order.transform.position = dest_one.position;
            order.transform.rotation = dest_one.rotation;
            destinationOne = true;
            //order[k] = order.gameObject.name;
            return;
        }
        */
        orders_on_hand[k].transform.parent = this.transform;
        orders_on_hand[k].transform.position = positions[k].transform.position;
        orders_on_hand[k].transform.rotation = positions[k].transform.rotation;
        k++;
    }

    public void Save()
    {
        GlobalControl.Instance.speed = agent.speed;
        GlobalControl.Instance.acceleration = agent.acceleration;
        GlobalControl.Instance.money = money;
    }

    void TestVectorFood()
    {
        if (!pos_one)
        {
            k = 0;
            orders_on_hand[k] = _temp;
            _temp = null;
            pos_one = true;
            return;
        }
        else if (!pos_two)
        {
            k = 1;
            orders_on_hand[k] = _temp;
            _temp = null;
            pos_two = true;
            return;
        }
        else if (!pos_three)
        {
            k = 2;
            orders_on_hand[k] = _temp;
            _temp = null;
            pos_three = true;
            return;
        }

    }

    void ResetBool(int index)
    {
        if(index == 0)
        {
            pos_one = false;
        }
        if (index == 1)
        {
            pos_two = false;
        }
        if (index == 2)
        {
            pos_three = false;
        }
    }
    void GetMoney()
    {
        on_path = false;
        game_manager.GetMoney(tableGameObject.GetComponent<TableManager>().tip_amount);
    }
}


