﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableManager : MonoBehaviour
{
    public Color color_to_order;

    GameObject money;

    public bool order = false;
    float random;
    float time_to_order;

    string n = "Money";

    public bool has_customer = false;
    public bool has_player = false;

    public bool has_tip;

    public int tip_amount;

    void Start()
    {
        money = this.transform.Find(n).gameObject;
    }

    private void Update()
    {
        if(has_tip)
        {
            money.SetActive(true);
        }
        else
        {
            money.SetActive(false);
        }
    }

}
