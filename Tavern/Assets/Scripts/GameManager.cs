﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{

    //-----------------PAUSE

    public GameObject pause;
    bool paused;

    //--------------SHOP-----------------------

    [Header("Shop")]
    public GameObject page_one, page_two, page_three, deal_page, assistant_page;
    public GameObject helper;
    public GameObject shop;
    public int speed_cost, speed_two;
    public int helper_cost;
    public int table_cost, table_cost_two;
    public int carry_cost, carry_two;
    public int food_cost, food_cost_two;
    public int dorm_cost;
    public int light_cost, light_cost_two;
    public int rug_cost;
    public int fireplace_cost, fireplace_cost_two, fireplace_cost_three;
    public int drink_cost, drink_cost_two;

    //--------------SHOP INTERFACE--------------
    public GameObject chicken,
                      beer,
                      dorm,
                      light_upg, light_upg2,
                      chair_and_table, chair_and_table2,
                      fireplace_one, fireplace_two,
                      velocidade_dois, forca_dois;

    //FMOD
    public static GameManager Instance;

    //------------------- Pedidos --------------
    [Header("Pedidos")]
    int food_index;

    public GameObject order_one;
    public GameObject order_two, order_three, order_four;

    public Transform[] spawners;

    int teste;

    public int order_number;

    //-------------------SPAWNS-----------------------
    [Header("Spawns")]
    public Transform[] tableSpawns;
    public Transform[] lights;
    public Transform[] plants;
    public Transform fireplace;
    public Transform rug;
    public Transform helper_spawn;

    //-----------------------------------------
    NavMeshSurface[] nav_surface;

    public GameObject shop_menu;
    Player player;

    Scene scene = new Scene();


    public Text dorms;
    GameObject money_text;
    public GameObject money_UI;

    public Transform spawner;
    bool enable = false;

    bool has_helper = false;

    [FMODUnity.EventRef]
    public string food_done;

    private void Awake()
    {
        // NavMeshBaker.instance.BuildNavMesh();
    }

    // Start is called before the first frame update
    void Start()
    {
        InstantSpawn();

        money_text = GameObject.Find("DinheiroTexto");
        money_UI = GameObject.Find("Money");
        money_UI.SetActive(false);

        dorm_cost = GlobalControl.Instance.dorms_start_cost;

        dorms.text = "Dorms" + "\n" + GlobalControl.Instance.dorm_current + " / " + GlobalControl.Instance.dorm_max;
        player = FindObjectOfType<Player>();
        money_text.GetComponent<Text>().text = "$" + player.money;
        //GlobalControl.Instance.Test();
        nav_surface = FindObjectsOfType<NavMeshSurface>();
        for (int i = 0; i < nav_surface.Length; i++)
        {
            nav_surface[i].BuildNavMesh();
        }

        pause = GameObject.Find("Pause");

        shop.SetActive(false);

        pause.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        //if (!enable) shop.SetActive(false);
        //if (enable) shop.SetActive(true);

        //speed_count.text ="Speed: " + player.GetComponent<NavMeshAgent>().speed.ToString();
        dorms.text = "Dorms" + "\n" + GlobalControl.Instance.dorm_current + " / " + GlobalControl.Instance.dorm_max;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
            {
                paused = true;
                Time.timeScale = 0;
                pause.SetActive(true);
                return;
            }

            if (paused)
            {
                paused = false;
                Time.timeScale = 1;
                pause.SetActive(false);
                return;
            }

        }



        if (Input.GetKeyDown(KeyCode.K))
        {
            player.money += 100;
            money_text.GetComponent<Text>().text = "$" + player.money;
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            DayEnd();
        }
    }


    /*
    public void Shop()
    {
        enable = !enable;
    }
    */

    public void FoodUpgrade()
    {
        if (player.money >= food_cost)
        {
            if (GlobalControl.Instance.food_one)
            {
                GlobalControl.Instance.food_one = false;
                GlobalControl.Instance.food_two = true;
                chicken.SetActive(true);
                player.money -= food_cost;
                money_text.GetComponent<Text>().text = "$" + player.money;
                money_UI.GetComponent<Text>().text = player.money.ToString();
                return;
            }
            else if (GlobalControl.Instance.food_two)
            {
                GlobalControl.Instance.food_two = false;
                GlobalControl.Instance.food_two = true;
                player.money -= food_cost;
                money_text.GetComponent<Text>().text = "$" + player.money;
                money_UI.GetComponent<Text>().text = player.money.ToString();
                return;
            }
            else if (GlobalControl.Instance.food_three)
            {
                return;
            }
        }
    }

    public void DrinkUpgrade()
    {
        if (player.money >= drink_cost)
        {
            if (GlobalControl.Instance.drink_one)
            {
                GlobalControl.Instance.drink_two = false;
                GlobalControl.Instance.drink_two = true;
                beer.SetActive(true);
                player.money -= drink_cost;
                money_text.GetComponent<Text>().text = "$" + player.money;
                money_UI.GetComponent<Text>().text = player.money.ToString();
                return;
            }
            else if (GlobalControl.Instance.drink_two)
            {
                GlobalControl.Instance.drink_two = false;
                GlobalControl.Instance.drink_three = true;
                player.money -= food_cost;
                money_text.GetComponent<Text>().text = "$" + player.money;
                money_UI.GetComponent<Text>().text = player.money.ToString();
                return;
            }
            else if (GlobalControl.Instance.drink_three)
            {
                return;
            }
        }
    }

    public void LightUpgrade()
    {
        if (GlobalControl.Instance.light_one && player.money >= light_cost)
        {
            GlobalControl.Instance.light_one = false;
            GlobalControl.Instance.light_two = true;
            light_upg.SetActive(true);
            player.money -= light_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
        else if (GlobalControl.Instance.light_two && player.money >= light_cost_two)
        {
            GlobalControl.Instance.light_two = false;
            GlobalControl.Instance.light_three = true;
            light_upg.SetActive(false);
            light_upg2.SetActive(true);
            player.money -= light_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
        else if (GlobalControl.Instance.light_three)
        {
            return;
        }
    }

    public void FireplaceUpgrade()
    {
        if (!GlobalControl.Instance.fireplace_one && !GlobalControl.Instance.has_fireplace && player.money >= fireplace_cost)
        {
            GlobalControl.Instance.fireplace_one = true;
            fireplace_one.SetActive(true);
            player.money -= fireplace_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
        }
        else if (GlobalControl.Instance.fireplace_one && player.money >= fireplace_cost_two)
        {
            GlobalControl.Instance.fireplace_one = false;
            GlobalControl.Instance.fireplace_two = true;
            GlobalControl.Instance.has_fireplace = true;
            fireplace_one.SetActive(false);
            fireplace_two.SetActive(true);
            player.money -= fireplace_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
        else if (GlobalControl.Instance.fireplace_two && player.money >= fireplace_cost_three)
        {
            GlobalControl.Instance.fireplace_two = false;
            GlobalControl.Instance.fireplace_three = true;
            player.money -= fireplace_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
        else if (GlobalControl.Instance.fireplace_three)
        {
            return;
        }
    }

    public void DormUpgrade()
    {
        if (player.money >= dorm_cost && GlobalControl.Instance.dorm_max < 10)
        {
            GlobalControl.Instance.dorm_max++;
            dorm.SetActive(true);
            player.money -= dorm_cost;
            dorm_cost += 25;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
    }

    public void Speed()
    {
        if (player.money >= speed_cost && GlobalControl.Instance.speed_bough < 3)
        {
            player.GetComponent<NavMeshAgent>().speed += 2;
            player.GetComponent<NavMeshAgent>().acceleration += 2;
            player.money -= speed_cost;
            GlobalControl.Instance.speed_bough++;
            speed_cost += 25;
            velocidade_dois.SetActive(true);
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
    }

    public void Table()
    {
        if (GlobalControl.Instance.level_one && player.money >= table_cost)
        {
            GlobalControl.Instance.level_one = false;
            GlobalControl.Instance.level_two = true;
            chair_and_table.SetActive(true);
            player.money -= table_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
        else if (GlobalControl.Instance.level_two && player.money >= table_cost_two)
        {
            GlobalControl.Instance.level_two = false;
            GlobalControl.Instance.level_three = true;
            player.money -= table_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
        else if (GlobalControl.Instance.level_three)
        {
            return;
        }
    }

    public void Rug()
    {
        if (player.money >= rug_cost)
        {
            if (!GlobalControl.Instance.rug)
            {
                GlobalControl.Instance.rug = true;
                player.money -= rug_cost;
                money_text.GetComponent<Text>().text = "$" + player.money;
                money_UI.GetComponent<Text>().text = player.money.ToString();
                return;
            }
        }
    }

    public void Carry()
    {
        if (player.money >= carry_cost && GlobalControl.Instance.max_orders < 3)
        {
            GlobalControl.Instance.max_orders++;
            Debug.Log(GlobalControl.Instance.max_orders);
            player.money -= carry_cost;
            carry_cost += 200;
            forca_dois.SetActive(true);
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
    }

    public void Helper()
    {
        if (player.money >= helper_cost && !GlobalControl.Instance.helper)
        {
            GlobalControl.Instance.helper = true;
            player.money -= helper_cost;
            money_text.GetComponent<Text>().text = "$" + player.money;
            money_UI.GetComponent<Text>().text = player.money.ToString();
            return;
        }
    }

    public void GetMoney(int other)
    {
        player.money += other;
        money_text.GetComponent<Text>().text = "$" + player.money;
    }

    public void DayEnd()
    {
        GetMoney(GlobalControl.Instance.dorm_current * 20);
        GlobalControl.Instance.dorm_current = 0;

        Time.timeScale = 0;

        
        
        if (GlobalControl.Instance.deal_given < GlobalControl.Instance.deal_needed)
        {
            SceneManager.LoadScene("GameOver");
            Destroy(GlobalControl.Instance.gameObject);
        }
        
        
        money_UI.SetActive(true);
        money_UI.GetComponent<Text>().text = player.money.ToString();

        shop_menu.SetActive(true);
        if (GlobalControl.Instance.light_two) light_upg.SetActive(true);
        if (GlobalControl.Instance.light_three) light_upg2.SetActive(true);
        if (GlobalControl.Instance.level_two) chair_and_table.SetActive(true);
        if (GlobalControl.Instance.level_three) chair_and_table2.SetActive(true);
        if (GlobalControl.Instance.fireplace_one) fireplace_one.SetActive(true);
        if (GlobalControl.Instance.fireplace_two) fireplace_two.SetActive(true);
        if (GlobalControl.Instance.dorm_max > 1) dorm.SetActive(true);
    }

    public void NextDay()
    {
        player.Save();
        SceneManager.LoadScene(GlobalControl.Instance.index);
        GlobalControl.Instance.dorm_current = 0;
        GlobalControl.Instance.index++;
    }


    public void Order(int order)
    {
        if (order == 1)
        {
            Invoke("Beer", 2);
        }

        if (order == 2)
        {
            Invoke("Soup", 4);
        }
    }

    public void NextPage()
    {
        page_one.SetActive(false);
        page_two.SetActive(true);
        if (GlobalControl.Instance.fireplace_one) fireplace_one.SetActive(true);
        if (GlobalControl.Instance.fireplace_two) fireplace_two.SetActive(true);
    }
    public void BackPage()
    {
        page_one.SetActive(true);
        page_two.SetActive(false);
        if (GlobalControl.Instance.light_two) light_upg.SetActive(true);
        if (GlobalControl.Instance.light_three) light_upg2.SetActive(true);
        if (GlobalControl.Instance.level_two) chair_and_table.SetActive(true);
        if (GlobalControl.Instance.level_three) chair_and_table2.SetActive(true);
    }

    void Beer()
    {
        GameObject temp;
        /*
        foreach (Transform item in spawners)
        {
            if(item.GetComponent<Spawner>().has_food == false)
            {
                temp = (GameObject)Instantiate(order_one, item.position, Quaternion.identity);
                temp.name = "Beer";
                item.GetComponent<Spawner>().has_food = true;
                FMODUnity.RuntimeManager.PlayOneShot(food_done);
                return;
            }
        }
        */
        temp = (GameObject)Instantiate(order_one, spawners[food_index].position, Quaternion.identity);
        temp.name = "Beer";
        FMODUnity.RuntimeManager.PlayOneShot(food_done);
        food_index++;
        if (food_index >= spawners.Length) food_index = 0;
        return;
    }

    void Soup()
    {
        GameObject temp;
        /*
        foreach (Transform item in spawners)
        {
            if (item.GetComponent<Spawner>().has_food == false)
            {
                temp = (GameObject)Instantiate(order_two, item.position, Quaternion.identity);
                temp.name = "Soup"; 
                item.GetComponent<Spawner>().has_food = true;
                FMODUnity.RuntimeManager.PlayOneShot(food_done);
                return;
            }
        }
        */

        temp = (GameObject)Instantiate(order_two, spawners[food_index].position, Quaternion.identity);
        temp.name = "Soup";
        FMODUnity.RuntimeManager.PlayOneShot(food_done);
        food_index++;
        if (food_index >= spawners.Length) food_index = 0;
        return;


    }

    void InstantSpawn()
    {
        //--------------------------MESAS

        if (GlobalControl.Instance.level_one)
        {
            for (int i = 0; i < tableSpawns.Length; i++)
            {
                Instantiate(Resources.Load("MesaInicial"), tableSpawns[i].position, Quaternion.Euler(0, 90, 0));
            }
        }
        else if (GlobalControl.Instance.level_two)
        {
            for (int i = 0; i < tableSpawns.Length; i++)
            {
                Instantiate(Resources.Load("MesaSecundaria"), new Vector3(tableSpawns[i].position.x, 3.59f, tableSpawns[i].position.z), Quaternion.Euler(0, 90, 0));
            }
        }
        else if (GlobalControl.Instance.level_three)
        {
            for (int i = 0; i < tableSpawns.Length; i++)
            {
                Instantiate(Resources.Load("MesaTercearia"), tableSpawns[i].position, Quaternion.Euler(0, 90, 0));
            }
        }

        //-------------------------LUZ

        if (GlobalControl.Instance.light_one)
        {
            for (int i = 0; i < lights.Length; i++)
            {
                Instantiate(Resources.Load("Luz1"), lights[i].position, Quaternion.Euler(0, 0, 0));
            }
        }
        else if (GlobalControl.Instance.light_two)
        {
            for (int i = 0; i < lights.Length; i++)
            {
                Instantiate(Resources.Load("Luz2"), lights[i].position, Quaternion.Euler(0, 90, 0));
            }
        }
        else if (GlobalControl.Instance.light_three)
        {
            for (int i = 0; i < lights.Length; i++)
            {
                Instantiate(Resources.Load("Luz3"), lights[i].position, Quaternion.Euler(0, 0, 0));
            }
        }

        //------------------------Lareira

        if (GlobalControl.Instance.fireplace_one)
        {
            Instantiate(Resources.Load("Fireplace1"), fireplace.position, Quaternion.Euler(0, 0, 0));
        }
        if (GlobalControl.Instance.fireplace_two)
        {
            Instantiate(Resources.Load("Fireplace2"), new Vector3(fireplace.position.x, 3.8f, fireplace.position.z), Quaternion.Euler(0, 0, 0));
        }
        if (GlobalControl.Instance.fireplace_three)
        {
            Instantiate(Resources.Load("Fireplace3"), new Vector3(fireplace.position.x, 0.13f, fireplace.position.z), Quaternion.Euler(0, 0, 0));
        }

        //--------------------------Tapete

        if (GlobalControl.Instance.rug)
        {
            Instantiate(Resources.Load("Rug"), rug.position, Quaternion.Euler(0, 90, 0));
        }

        //----------------------------Ajudante

        if (GlobalControl.Instance.helper)
        {
            Instantiate(Resources.Load("HelperAIPurchased"), helper_spawn.position, Quaternion.Euler(0, 0, 0));
        }
    }

    public void DealPage()
    {
        deal_page.SetActive(true);
        page_one.SetActive(false);
        page_two.SetActive(false);
        page_three.SetActive(false);
        assistant_page.SetActive(false);
    }

    public void SkillsPage()
    {
        deal_page.SetActive(false);
        page_one.SetActive(false);
        page_two.SetActive(false);
        assistant_page.SetActive(false);
        page_three.SetActive(true);
        if (GlobalControl.Instance.max_orders > 1) forca_dois.SetActive(true);
        if (GlobalControl.Instance.speed_bough > 1) velocidade_dois.SetActive(true);

    }

    public void FurniturePage()
    {
        deal_page.SetActive(false);
        page_one.SetActive(true);
        page_two.SetActive(false);
        page_three.SetActive(false);
        assistant_page.SetActive(false);
    }
    public void AssistantPage()
    {
        deal_page.SetActive(false);
        page_one.SetActive(true);
        page_two.SetActive(false);
        page_three.SetActive(false);
        assistant_page.SetActive(true);
    }

    public void NavmeshBuildTable()
    {
        for (int i = 0; i < nav_surface.Length; i++)
        {
            nav_surface[i].BuildNavMesh();
        }
    }

    public void Resume()
    {
        paused = false;
        Time.timeScale = 1;
        pause.SetActive(false);
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu");
        Destroy(GlobalControl.Instance.gameObject);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
