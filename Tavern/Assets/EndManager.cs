﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class EndManager : MonoBehaviour
{
    void Start()
    {
        Invoke("Menu", 4);
    }

    void Menu()
    {
        SceneManager.LoadScene("Menu");
    }
}
